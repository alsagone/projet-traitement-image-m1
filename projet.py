
import helper
import traitement
import cv2
import sys

eye_cascade_file = helper.get_fichier(
    'haarcascade_eye_tree_eyeglasses.xml')

fichier_lunettes = helper.get_fichier('sunglasses.png')

video_neige = helper.get_fichier('neige.mp4')
image_neige = helper.get_fichier('fond_neige.jpg')


class Webcam:
    def __init__(self) -> None:
        self.eye_cascade = cv2.CascadeClassifier(eye_cascade_file)
        self.image_lunettes = cv2.imread(
            fichier_lunettes, cv2.IMREAD_UNCHANGED)
        self.image_neige = cv2.imread(image_neige)

        self.video_neige = video_neige

        self.sepia = False
        self.lunettes = False
        self.neige = False
        self.fond_neige = False

    def traiter_frame(self, frame_video: cv2.Mat, frame_neige: cv2.Mat = []) -> cv2.Mat:
        '''Résumé: applique les différents effets activés par l'utilisateur

        Paramètres:
            frame_video (cv2.Mat): la frame issue de la capture de la webcam
            frame_neige (cv2.Mat, optionnel): la frame contenant de la neige - vide ([]) par défaut.

        Retourne:
            cv2.Mat: l'image sur laquelle les effets ont été appliqués
        '''
        if self.fond_neige:
            frame_video = traitement.incruster_fond(
                frame_video, self.image_neige)

        if self.lunettes:
            frame_video = traitement.mettre_lunettes(
                frame_video, self.eye_cascade, self.image_lunettes)

        if self.neige:
            frame_video = traitement.incruster_neige(frame_video, frame_neige)

        if self.sepia:
            frame_video = traitement.filtre_sepia(frame_video)

        return frame_video

    def gerer_effets(self, effet: str):
        '''Résumé: fonction appelée à chaque clic sur les boutons de l'interface
        Gère l'activation/désactivation des effets et affiche un message de log

        Paramètre:
            effet (str): l'effet à gérer
        '''
        if effet == 'Sepia':
            self.sepia = not (self.sepia)
            log_message = 'Filtre sepia '
            log_message += 'activé' if self.sepia else 'désactivé'

        elif effet == 'Lunettes':
            self.lunettes = not (self.lunettes)
            log_message = 'Lunettes '
            log_message += 'activées' if self.lunettes else 'désactivées'

        elif effet == 'Neige':
            self.neige = not (self.neige)
            log_message = 'Neige '
            log_message += 'activée' if self.neige else 'désactivée'

        elif effet == 'Fond neige':
            self.fond_neige = not (self.fond_neige)
            log_message = 'Fond neige '
            log_message += 'activé' if self.fond_neige else 'désactivé'

        elif effet == 'Désactiver':
            self.sepia = False
            self.lunettes = False
            self.neige = False
            self.fond_neige = False
            log_message = 'Effets tous désactivés'

        else:
            print(f'Effet inconnu: "{effet}"', file=sys.stderr)
            return

        print(log_message)
        return

    def button_listener(self, event: int, x: int, y: int, flags, userdata):
        '''Résumé: fonction qui gère le clic sur les boutons de l'interface

        Paramètres:
            event (int): le code de l'événement capturé par OpenCV
            x (int): la coordonnée X de la souris lors de la survenue de l'événement
            y (int): la coordonnée Y de la souris lors de la survenue de l'événement
            userdata (np.array): la taille de la fenêtre
        '''
        if not (event == cv2.EVENT_LBUTTONDOWN) or not (helper.est_compris(y, 0, 15)):
            return

        if helper.est_compris(x, 0, 100, strict=True):
            self.gerer_effets('Sepia')

        elif helper.est_compris(x, 100, 200):
            self.gerer_effets('Lunettes')

        elif helper.est_compris(x, 200, 300, strict=True):
            self.gerer_effets('Neige')

        elif helper.est_compris(x, 300, 400, strict=True):
            self.gerer_effets('Fond neige')

        elif helper.est_compris(x, userdata.shape[1]-100, userdata.shape[1], strict=True):
            self.gerer_effets('Désactiver')
        return

    def capture(self, enregistre: bool = False):
        cv2.namedWindow('Webcam')
        flux_webcam = cv2.VideoCapture(0)

        if flux_webcam.isOpened():
            ret_webcam, frame_webcam = flux_webcam.read()

        else:
            print(f"Impossible d'ouvrir la webcam", file=sys.stderr)
            sys.exit(1)

        if enregistre:
            width = int(flux_webcam.get(cv2.CAP_PROP_FRAME_WIDTH))
            height = int(flux_webcam.get(cv2.CAP_PROP_FRAME_HEIGHT))
            writer = cv2.VideoWriter(
                'enregistrement.mp4', cv2.VideoWriter_fourcc(*'mp4v'), 24, (width, height))

        flux_neige = cv2.VideoCapture(self.video_neige)

        if flux_neige.isOpened():
            ret_neige, frame_neige = flux_neige.read()

        else:
            print("Impossible d'ouvrir la vidéo neige", file=sys.stderr)
            sys.exit(1)

        while ret_webcam:
            cv2.setMouseCallback('Webcam', self.button_listener, frame_webcam)
            ret_neige, frame_neige = flux_neige.read()

            # Fait boucler la vidéo neige si on arrive au bout
            if not (ret_neige):
                flux_neige.set(cv2.CAP_PROP_POS_FRAMES, 0)
                ret_neige, frame_neige = flux_neige.read()

            helper.draw_menu(frame_webcam)

            frame_modifiee = self.traiter_frame(frame_webcam, frame_neige)

            if enregistre:
                writer.write(frame_modifiee)

            cv2.imshow('Webcam', frame_modifiee)
            ret_webcam, frame_webcam = flux_webcam.read()

            key = cv2.waitKey(20)
            if key == 27:  # fermer la fenêtre avec Échap
                break

        flux_webcam.release()
        cv2.destroyAllWindows()
        return


if __name__ == '__main__':
    w = Webcam()
    w.capture(enregistre=False)
