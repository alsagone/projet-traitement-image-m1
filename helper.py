import platform
import cv2
import numpy as np


def get_fichier(nom_fichier: str) -> str:
    ''' Résumé:
        Récupère le chemin du nom de fichier passé en paramètre
        (en prenant en compte la plateforme sur laquelle le programme tourne)

        Paramètres:
        nom_fichier (str): le nom du fichier

        Retourne:
        str: le chemin complet du fichier dans le dossier assets/
    '''
    separateur = '\\' if platform.system == 'Windows' else '/'
    return f'assets{separateur}{nom_fichier}'


def est_compris(x: int, a: int, b: int, strict: bool = False) -> bool:
    ''' Résumé:
        Retourne un booléen indiquant si x est compris entre a et b
        On peut rendre la comparaison stricte ou non 

        Paramètres:
        x (int): l'entier à comparer
        a (int): une des deux bornes
        b (int): l'autre borne
        strict (bool, optionnel): booléen indiquant si la comparaison doit être stricte ou non - vaut False par défaut

        Retourne:
        bool: un booléen indiquant si x est compris entre a et b
    '''
    min_val, max_val = min(a, b), max(a, b)

    if strict:
        return min_val < x < max_val

    return min_val <= x <= max_val


def draw_rectangle(x1: int, x2: int, texte: str, img: cv2.Mat, y1: int = 0, y2: int = 20):
    ''' Résumé: dessine un rectangle avec du texte sur l'image et aux coordonnées indiquées

        Paramètres:
        x1 (int): coordonnée du point de départ du rectangle sur l'axe X
        x2 (int): coordonnée du point de fin du rectangle sur l'axe X
        texte (str): le texte à écrire
        img (cv2.Mat): l'image sur laquelle dessiner le rectangle
        y1 (int, optionnel): coordonnée du point de départ du rectangle sur l'axe Y - Vaut 0 par défaut.
        y2 (int, optionnel): coordonnée du point de départ du rectangle sur l'axe Y - Vaut 20 par défaut.
    '''

    font = cv2.FONT_HERSHEY_SIMPLEX
    org = (50, 10)
    fontScale = 0.5
    # Blue color in BGR
    text_color = (255, 0, 0)
    thickness = 1
    box_color = (127, 127, 127)
    rect = cv2.rectangle(img, (x1, y1), (x2, y2), box_color, cv2.FILLED)
    cv2.putText(rect, texte, (x1, 15), font, fontScale,
                text_color, thickness, cv2.LINE_AA)

    return


def draw_menu(img: cv2.Mat):
    ''' Résumé:
        Dessine l'interface sur l'image passée en paramètre

        Paramètres:
        img (cv2.Mat): l'image sur laquelle dessiner l'interface
    '''
    x1, x2 = 0, 100
    draw_rectangle(x1, x2, 'Sepia', img)
    draw_rectangle(x1+100, x2+200, 'Lunettes', img)
    draw_rectangle(x1+200, x2+300, 'Flocons', img)
    draw_rectangle(x1+300, x2+400, 'Fond neige', img)
    draw_rectangle(img.shape[1]-100, img.shape[1], 'Retirer effets', img)
    return


def combiner_pixel(pixel_avant_plan: np.ndarray, pixel_arriere_plan: np.ndarray, alpha: float) -> np.ndarray:
    '''
    Résumé: Combine le pixel_foreground avec le pixel_background étant donné une valeur d'alpha

    Paramètres:
        pixel_avant_plan (np.ndarray): le pixel du avant plan
        pixel_arriere_plan (np.ndarray): le pixel d'arrière plan
        alpha (float): la valeur d'alpha

    Retourne:
        np.ndarray: la combinaison des deux pixels
    '''
    return alpha * pixel_avant_plan + (1 - alpha) * pixel_arriere_plan
