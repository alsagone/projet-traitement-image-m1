import cv2
import numpy as np
import helper


def filtre_sepia(img: cv2.Mat) -> cv2.Mat:
    ''' Résumé:
        Applique un filtre sépia à l'image passée en paramètre

        Paramètres:
        img (cv2.Mat): une image

        Retourne:
        cv2.Mat: une image sur laquelle on a appliqué un filtre sépia
    '''
    sepia_kernel = np.matrix(
        [[0.272, 0.543, 0.131], [0.349, 0.686, 0.168], [0.393, 0.769, 0.189]])

    sepia_image = cv2.transform(img, sepia_kernel)

    # On fait attention à ne pas dépasser 255
    sepia_image[np.where(sepia_image > 255)] = 255
    return np.array(sepia_image)


def integrer_lunettes(img: cv2.Mat, paire_yeux: np.array, lunettes: cv2.Mat) -> cv2.Mat:
    ''' Résumé:
        Intègre des lunettes sur l'image, étant donné les coordonnées de la paire d'yeux passée en paramètre

        Paramètres:
        img (cv2.Mat): une image
        paire_yeux: une paire de coordonnées d'yeux
        lunettes: l'image des lunettes à incruster

        Retourne:
        cv2.Mat: une image sur laquelle on a mis des lunettes sur la paire d'yeux
    '''

    # Si on n'a pas une paire d'yeux on ne fait rien
    if paire_yeux.shape[0] != 2:
        return img

    # On trie les yeux de telle sorte que l'oeil gauche (celui avec la coordonnée x la plus petite) soit le premier
    oeil_gauche, oeil_droit = sorted(paire_yeux, key=lambda x: x[0])

    gauche_x, gauche_y, gauche_w, gauche_h = oeil_gauche
    droit_x, droit_y, droit_w, droit_h = oeil_droit

    # On peut calculer la taille des lunettes:
    # Elles se terminent au coin inférieur droit de l'oeil droit
    # On peut récupérer cette coordonnée en faisant droit_x + droit_w
    # Elles commencent au coin inférieur gauche de l'oeil gauche, soit gauche_x
    # Donc on trouve la largeur des lunettes en soustrayant ces deux valeurs

    lunettes_largeur = droit_x + droit_w - gauche_x
    lunettes_hauteur = max(gauche_h, droit_h)

    lunettes = cv2.resize(
        lunettes, (lunettes_largeur, lunettes_hauteur), interpolation=cv2.INTER_AREA)

    # Maintenant on peut placer les lunettes sur l'image
    lunettes = lunettes.astype(float)

    for i in range(lunettes_hauteur):
        for j in range(lunettes_largeur):

            pixel_lunettes = lunettes[i, j][:3]
            pixel_webcam = img[gauche_y+i, gauche_x+j]
            alpha = lunettes[i, j][3] / 255.
            p = helper.combiner_pixel(
                pixel_lunettes, pixel_webcam, alpha).astype(int)

            img[gauche_y+i, gauche_x+j] = p

    return img


def mettre_lunettes(img: cv2.Mat, eye_cascade, lunettes: cv2.Mat):
    ''' Résumé:
        Prend une image, détecte s'il y a des yeux et leur met une paire de lunettes

        Paramètres:
        img (cv2.Mat): une image
        eye_cascade: la cascade de Haar à utiliser
        lunettes (cv2.Mat): l'image des lunettes à incruster

        Retourne:
        cv2.Mat: une image sur laquelle on a mis des lunettes sur la paire d'yeux
    '''

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    yeux = eye_cascade.detectMultiScale(gray, 1.1, 4)

    # yeux contient la liste de tous les yeux détectés
    # Au format [x, y, w, h]
    # (x,y) coordonnées du point inférieur gauche
    # (w,h) largeur/hauteur de l'oeil

    # Si on ne détecte aucun oeil, eyes est de type 'tuple'
    if type(yeux) is tuple or yeux.shape[0] % 2 != 0:
        return img

    split_paires = np.array_split(yeux, np.arange(2, len(yeux), 2))

    for paire_yeux in split_paires:
        img = integrer_lunettes(img, paire_yeux, lunettes)

    return img


def incruster_neige(frame_video: cv2.Mat, frame_neige: cv2.Mat) -> cv2.Mat:
    ''' Résumé:
        Applique la neige sur la frame_video

        Paramètres:
        frame_video (cv2.Mat): une image sur laquelle appliquer la neige
        frame_neige (cv2.Mat): une image contenant de la neige

        Retourne:
        cv2.Mat: une image sur laquelle on a mis des lunettes sur la paire d'yeux
    '''

    # Le masque = tous les pixels entre le couleur (0,0,0) et (240,240,240)
    min_range = np.array([0, 0, 0])
    max_range = np.array([240, 240, 240])
    mask = cv2.inRange(frame_neige, min_range, max_range)

    # On enlève ces pixels de l'image de la neige
    f = frame_neige - cv2.bitwise_and(frame_neige, frame_neige, mask=mask)

    # Et on les remplace par la vidéo
    return np.where(f == 0, frame_video, f)


def incruster_fond(frame_video: cv2.Mat, fond_neige: cv2.Mat) -> cv2.Mat:
    ''' Résumé:
        Change le background de la frame_video par le fond_neige

        Paramètres:
        frame_video (cv2.Mat): une image sur laquelle appliquer la neige
        fond_neige (cv2.Mat): l'image de fond

        Retourne:
        cv2.Mat: une image sur laquelle on a remplacé le background par le fond
    '''
    max_range = np.array([255, 255, 255])
    min_range = np.array([140, 140, 140])
    mask = cv2.inRange(frame_video, min_range, max_range)
    f = frame_video - cv2.bitwise_and(frame_video, frame_video, mask=mask)
    return np.where(f == 0, fond_neige, f)
