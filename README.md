# Projet Traitement Image M1

Abdouroihamane Hakim - Belhadj Lina

## Principe

Le programme ouvre le flux vidéo de votre webcam et vous pouvez appliquer plusieurs effets:

- Filtre sépia: applique un filtre sépia à votre vidéo
- Filtre lunettes de soleil: met une paire de lunettes sur chaque visage détecté
- Filtre neige: fait tomber de la neige sur votre vidéo
- Changer le fond de votre vidéo par un fond enneigé

## Installation

- Clonez ce dépôt git: `git clone https://gitlab.com/alsagone/projet-traitement-image-m1.git`
- Placez-vous dans le dossier `projet-traitement-image-m1`
- Installez toutes les dépendances avec `pip install -r requirements.txt`
- Lancez le programme avec `python projet.py`

## Utilisation

Vous pouvez activer/désactiver les effets en cliquant sur les boutons en haut de la fenêtre.
Pour stopper complètement le programme, vous n'avez qu'à appuyer sur la touche Echap.
